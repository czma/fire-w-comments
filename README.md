# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

FIRE code from https://tavazoielab.c2b2.columbia.edu/lab/
with added comments.

###sample output###
Original FIRE output

perl FIRE-1.1a/fire.pl --expfiles netprophet_scores/YNL021W --exptype=continuous --fastafile_dna=s_cerevisiae.promoters.fasta --seqlen_dna=600 --nodups=1

Thu Oct 23 12:11:34 CDT 2014

DNA, remove duplicates, create netprophet_scores/YNL021W_FIRE/DNA/YNL021W

DNA, quantizing.

DNA, Step 1: seed discovery. **(fire.pl  get_cmd_mifind)**

Setting -shuffle to 8192.  **(mi_find.c)**

Expression file is a 6219 by 1 matrix.

Min expression value read is 0.00, max is 0.05

Reading sequences ... Done  **(from the fasta file)**

Number of ORFs = 6202

Number of bins for expression data will be 62.

Calculating Mutual Information for all seeds ... Done

Determining threshold ...

Evaluating seed #0, AAGTCCG ... not passed.

Decreasing intervals phase.

Searches for 10 consecutive 'not passed'.

Evaluating seed #9, TGTGTCC ... not passed.

Evaluating seed #8, AACTCTG ... not passed.

Evaluating seed #7, TCGGCTC ... not passed.

Evaluating seed #6, AAGCTAT ... not passed.

Evaluating seed #5, GTACTCC ... not passed.

Evaluating seed #4, TCAAAAA ... not passed.

Evaluating seed #3, ACCTTTG ... not passed.

Evaluating seed #2, TAACGCA ... not passed.

Evaluating seed #1, ATTACTC ... passed.

Evaluating seed #11, CTTTCAG ... not passed.

Evaluating seed #10, ACTCCCA ... not passed.

Evaluating seed #9, TGTGTCC ... not passed.

Evaluating seed #8, AACTCTG ... not passed.

Evaluating seed #7, TCGGCTC ... not passed.

Evaluating seed #6, AAGCTAT ... not passed.

Evaluating seed #5, GTACTCC ... not passed.

Evaluating seed #4, TCAAAAA ... not passed.

Evaluating seed #3, ACCTTTG ... not passed.

Evaluating seed #2, TAACGCA ... not passed. **(ten consecutive 'not passed,' all seeds after are ignored)**

Threshold set to seed #1 (included).

AAGTCCG	0.0140	0.000

ATTACTC	0.0135	-0.000

DNA, Step 2: seed optimization.  **(fire.pl  get_cmd_mioptimize)**

Using minr = 5.0000.

Motif length will be 9.

Creating prefix tree for storing patterns ... Done.

Reading expression data ... Done.

Creating hash table for gene names ... Done.

Reading sequences and building the index.

130839 k-mers in index

Matrix occupancy = 3485311 / 811463478.000000

Done.                                



Evaluating seed AAGTCCG ... 

optimizing.

-343932928	-452919808	6202	2	62

Best optimized motif: .AAGTCCG.

Mutual information: 0.013857

Optimization stability: 10

Z-score: 4.286

Starting seed: AAGTCCG

Seed MI: 0.013857



Evaluating seed ATTACTC ... 

Motif .AAGTCCG. : I(B;E|A)/I(A;B)=300.2794

optimizing.

-310222848	-452919808	6202	2	62

Best optimized motif: .ATTACTC[ACT]

Mutual information: 0.013777

Optimization stability: 10

Z-score: 4.754

Starting seed: ATTACTC

Seed MI: 0.013192

DNA, Step 3: evaluation of motif significances.

number of bins for expression data 62

Processing motif .AAGTCCG..

MI=0.014

Lin reg coef=0.000

Shuffle rank=2

Z-score=4.277

Sigificance category= NOT-SIGNIF

Robustness (jn_f=3)=0/10



Processing motif .ATTACTC[ACT].

MI=0.014

Lin reg coef=-0.000

Shuffle rank=1

Z-score=4.777

Sigificance category= NOT-SIGNIF

Robustness (jn_f=3)=0/10



DNA, Step 3.5: creating binary expression profiles.

DNA, Step 4: discovery of distance constraints.

Read 2 motifs.

Read 6200 (genes) x 2 (conditions) matrix.

Read 6219 (genes) x 1 (conditions) matrix.

Reading sequences and building motif profiles ...Done.

Average sequence length is 601.

Quantized E vector into 62 bins

Motif .AAGTCCG., present in 181 genes.

       Average distance: MI=0.0000, rank=10000/10000, Z=0.0000, mbins=2

  Orientation bias {5'): MI=0.0088, rank=3225/10000, Z=0.4410

  Orientation bias (3'): MI=0.0067, rank=9043/10000, Z=-1.2757

       Copy number (1+): 181 genes, MI=0.0061, rank=9001/10000, Z=-1.2498

       Copy number (2+): 3 genes, MI=0.0021, rank=10000/10000, Z=-0.2459



Quantized E vector into 62 bins

Motif .ATTACTC[ACT], present in 408 genes.

       Average distance: MI=0.0000, rank=10000/10000, Z=0.0000, mbins=2

  Orientation bias {5'): MI=0.0048, rank=9896/10000, Z=-2.0384

  Orientation bias (3'): MI=0.0094, rank=1161/10000, Z=1.2129

       Copy number (1+): 408 genes, MI=0.0050, rank=9740/10000, Z=-1.7504

       Copy number (2+): 15 genes, MI=0.0053, rank=8441/10000, Z=-0.5644

       Copy number (3+): 1 genes, MI=0.0010, rank=10000/10000, Z=-0.1209



DNA, Step 5: summarize information.

Reading DNA motifs mi_dist output ...Done.

Parsing DNA motif file ... Done.

Reading mi_signif output file for DNA motifs ...Done.

Got 0 motifs

Writing good columns to netprophet_scores/YNL021W_FIRE/DNA/YNL021W.signifcolumns ...Done.

Outputing summary ... Done.

HI!HELLO!netprophet_scores/YNL021W_FIRE/DNA/YNL021W.summary.binary created.

DNA, Step 5.6: generate profiles for motifs in summary file.

DNA, Determining target gene sets.

No motifs.

DNA, Drawing position histograms.

defined(@array) is deprecated at /Users/CZMa/Desktop/BrentLabRotation/FIRE-1.1a/SCRIPTS/draw_position_histogram.pl line 134.

(Maybe you should just omit the defined()?)

Table.pm: cannot open file "netprophet_scores/YNL021W_FIRE/DNA/YNL021W.targets" ..

DNA, Step 6: cluster motifs.

Using positive correlation.

No motifs in summary file.

DNA, Step 5.6: generate genome-wide GO enrichments for motifs in summary file.

Option goindex requires an argument

Option gonames requires an argument

Table.pm: cannot open file "netprophet_scores/YNL021W_FIRE/DNA/YNL021W.clusters" ..

DNA, draw motif maps for co-localizing pairs

Table.pm: cannot open file "netprophet_scores/YNL021W_FIRE/DNA/YNL021W.mimatrix" ..

DNA, Step 7: draw matrix figure.

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.clusters does not exist, ignoring.

Now doing the graphical display.

No motifs in @MOTIFS ...

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.clusters does not exist, ignoring.

Now doing the graphical display.

No motifs in @MOTIFS ...

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.clusters does not exist, ignoring.

Now doing the graphical display.

No motifs in @MOTIFS ...

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.clusters does not exist, ignoring.

Now doing the graphical display.

No motifs in @MOTIFS ...

DNA, Step 7b: some distance stuff.

Parsing distance report file ...  Done.

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.fullmimatrix does not exists. at /Users/CZMa/Desktop/BrentLabRotation/FIRE-1.1a/SCRIPTS/Sets.pm line 165.

netprophet_scores/YNL021W_FIRE/DNA/YNL021W.fullmimatrix does not exists. at /Users/CZMa/Desktop/BrentLabRotation/FIRE-1.1a/SCRIPTS/Sets.pm line 165.

DNA, Step 10: motif report.

Thu Oct 23 12:12:38 CDT 2014

No RNA sequence data. Perhaps you need to download the relevant 

species-specific data file from http://tavazoielab.princeton.edu/FIRE.